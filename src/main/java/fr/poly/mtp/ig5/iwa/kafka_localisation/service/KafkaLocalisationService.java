package fr.poly.mtp.ig5.iwa.kafka_localisation.service;


import fr.poly.mtp.ig5.iwa.kafka_localisation.config.KafkaConfig;
import fr.poly.mtp.ig5.iwa.kafka_localisation.entity.LocalisationDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaLocalisationService {


    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    public void saveLocalisation(LocalisationDAO localisationDAO)
    {
        System.out.println(localisationDAO);

        this.kafkaTemplate.send(KafkaConfig.TOPIC, localisationDAO);
    }

    @KafkaListener(topics = KafkaConfig.TOPIC,
            groupId = "group_id")
    public void consume(LocalisationDAO localisationDAO)
    {
        System.out.println("Localisation : " + localisationDAO);

    }




}
