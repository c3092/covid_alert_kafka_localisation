package fr.poly.mtp.ig5.iwa.kafka_localisation.controller;

import fr.poly.mtp.ig5.iwa.kafka_localisation.entity.LocalisationDAO;
import fr.poly.mtp.ig5.iwa.kafka_localisation.service.KafkaLocalisationService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.net.InetAddress;

@RestController
@RolesAllowed("user")
@RequestMapping("/localisation_kafka")
public class KafkaLocalisationController {


    private final KafkaLocalisationService localisationKafkaService;


    KafkaLocalisationController(KafkaLocalisationService localisationKafkaService) {
        this.localisationKafkaService = localisationKafkaService;
    }


    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void addLocalisation(@RequestBody LocalisationDAO newLocalisationDAO) {

        localisationKafkaService.saveLocalisation(newLocalisationDAO);
    }

    @GetMapping("/ping")
    public boolean getPing(){
        try{
            String KAFKA_URL = System.getenv("KAFKA_URL");
            String[] KAFKA_IP = KAFKA_URL.split(":");
            InetAddress address = InetAddress.getByName(KAFKA_IP[0]);
            return address.isReachable(2000);
        } catch (Exception e){
            return false;
        }
    }



}
