package fr.poly.mtp.ig5.iwa.kafka_localisation.entity;

import java.math.BigDecimal;
import java.util.Date;

public class LocalisationDAO {


    private BigDecimal latitude;
    private BigDecimal longitude;
    private Date location_date;
    private String user;




    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public Date getLocation_date() {
        return location_date;
    }

    public void setLocation_date(Date location_date) {
        this.location_date = location_date;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Localisation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", location_date=" + location_date +
                ", user='" + user + '\'' +
                '}';
    }

}
