package fr.poly.mtp.ig5.iwa.kafka_localisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fr")
public class ApplicationKafkaLocalisation {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationKafkaLocalisation.class, args);
    }

}
