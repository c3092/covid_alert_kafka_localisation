package fr.poly.mtp.ig5.iwa.kafka_localisation.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.common.config.TopicConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaConfig {

    public static final String TOPIC = "localisation-topic";
    @Bean
    public NewTopic myTopic() {
        return TopicBuilder.name(TOPIC)
                .partitions(4)
                .replicas(1)
                .config(TopicConfig.RETENTION_MS_CONFIG, "1680000000")
                .build();
    }

}
